## Introduction to Framework

The framework is designed based on Behavior Driven Development (BDD) using Cucumber-JVM. For more about Cucumber. Its also uses Spring framework capabilities which are already suplied by `Cucumber-spring`. You don't need to do any additional configurations. All the configurations are present within the directory `\src\test\java\<package>\SpringConfig`. Please do not modify these files, it may break complete framework. The purpose of using Spring Framework is to maintain the dependence injection throught the test context and also allow cucumber to share variables between steps.

This framework is designed in such a way that you can automate test scenarios to that integrates multiple interfaces such as SOAP, REST, JDBC and WEB UI. You can initiate your tests from any interface and end up in another interface. By this methodology you can design end to end scenarios instead of restricting to just Web Interface. Behaviour Driven Development is the main driver to execute your test scenarios. And the framework uses Cucumber to drive those scenarios. 

Below are the capabilities of this framework

* Multi Browser Support
* Multi Environment Support
* Multi Interface Support
* Running Tests without a browser
* Parallel Execution of Tests
* Uses Selenium 3.x
* Page Object and Page Factory Design
* Integrates with SOAP, REST, JDBC
* Parse XML and JSON Data
* Test data from Excel/CSV/Cucumber
* Easy to Configure or Plug and Play
* Easy to integrate in CI/CD Pipeline
* Spring Integration
* Graphical Test Reports
* Usability and Readability 

## Installation and Setup

To Setup this project you need to install below softwares and configure it properly. Make sure you have `JVM_HOME` and `M2_HOME` Configured properly.

1. Java JDK 1.8
2. Maven 3.X
3. IntelliJ/Eclipse (Install Cucumber plugins)
4. Git/SourceTree

We highly recommend to use Intellij Community version for faster and easy development. You can download it from [here](https://www.jetbrains.com/idea/download/) for free.

Installation of this project is a same as any other Java Maven project. Clone the project and do `mvn clean install` which will download all the dependencies and then executes Cucumber Tests.

You can also run your project by right click on CukeRunner file under `/src/test/java/<package>/CukeRunner.Java` and click Run.

The project will automatically download the WebDriver to local project folder when ran. By default the tests are ran in chrome browser and you can change this configuration in the file `src\main\java\resources\webdriver.properties` and look for `default.webbrowser`

## Framework and Project Structure
The project `src/main/java` consists of all the Application and Library resources and `src/main/test` consists of all cucumber tests. All the classes in `src/main/java` is configured as Spring component so that you can Autowire in the test classes. Please find the folder structure below.
````
src
  ├───main
     	├───java
 		│   └───com.company.project
 		│   │       ├───data
 		│   │       ├───library
 		│   │       ├───pages
 		│   │       └───webdriver
 		│   └───resources
 		└───test
 			├───java
 			│   └───com.company.project
 			│       ├───SpringConfig
 			│       └───StepDefinitions
 			└───resources
 				├───features
 				└───json

````
### Application/Library Sources
The application library sources `src\main\java` consists of all the project libraries related to webdriver, page object, data and library

#### Webdriver Folder
This folder is responsible for creating Webdriver object. It was implemented with Factory design pattern such that based on the chosen browser type it will create the desired webdriver instance. 

`DriverManager.java` is the class that downloads the webdriver executable file in project directory. When you run your Tests for the first time, it will download the executable driver file based on the browser type and creates Webdriver instance. From the next run onwards, it will check whether the driver file exists or not. If exists it will create Webdriver instance otherwise then it will redownload the executable driver. If you don't want to download and configure a proper location you can change this by altering `driverFile` variable within this class

`DriverManagerFactory.java` is the factory class which consists of switch case statement with browser names. When we set the `default.webbrowser` in the resources `webdriver.properties` file, it will create that browser webdriver instance. This class is already configured to create chrome driver by default if you don't set anything in the `webdriver.properties` file.

For each type of browser type you have individual driver class which segregates the creation of object type. If you want to implement your own driver type you need to create a new class that extends the `DriverManager` or simply you clone any existing class and modify the driver settings accordingly.

#### Pages Folder
This folder consists of Page Objects for all Web UI Automation. `BaseClass.java` is the main parent class which extents all other clases within the project. BasePage constructer initialization of webdriver instance and it also initiates PageFactory web elements. This class consists of all common functions that can be used in other clases. Most importantly all the wait methods will present in this class. 

This `BaseClass` will be extended by all other classes. So the `BaseClass` is parent class for all your page objects. If you want to create method to utilise in all the Java page objects the you should place it in `BaseClass` instead of keeping it in every other page object class.

#### Library Folder

This folder consists of all the common methods or utility functions



