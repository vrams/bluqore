package com.kenopsy.automation.webdriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ChromeDriverManager extends DriverManager {

    private static Logger logger = LoggerFactory.getLogger(ChromeDriverManager.class);
    private ChromeOptions chromeOptions = new ChromeOptions();

    /******************************************************************************/
    /* This constructor sets Chrome Web Driver URL from application.properties    */
    /* It also sets zip filename and webdriver executable file name. These values */
    /* and sets executable file name. These names are used to download WebDriver  */
    /******************************************************************************/
    public ChromeDriverManager()
    {
        System.setProperty("webdriver.chrome.silentOutput", "true");
        //System.setProperty("webdriver.chrome.verboseLogging", "true");
        if(getOsName().contains("windows"))
        {
            setURL(webDriverOptions.getString("chrome.windows.url"));
            setZIPFilename(new File(project +"chromedriver.zip"));
            setEXEFileName(new File(project + "chromedriver.exe"));
        }
        else if(getOsName().contains("linux"))
        {
            setURL(webDriverOptions.getString("chrome.linux.url"));
            setZIPFilename(new File("chromedriver.zip"));
            setEXEFileName(new File(project + "chromedriver"));
        }
        System.setProperty("webdriver.chrome.driver", getEXEFilePath());
        createDriver();
    }

    /******************************************************************************/
    /* This Method creates chrome WebDriver Object. This instance will be created */
    /* during initialization of spring context. Refer getWebDriver() method in    */
    /* Spring configuration file springconfig/CucumberContext.java file           */
    /******************************************************************************/
    @Override
    public void createDriver()
    {
        downloadDrivers();
        setChromeArguments();
        setHeadless();
        //setChromeExtensions();
        driver = new ChromeDriver(chromeOptions);
    }

    /******************************************************************************/
    /* This Method used to set chrome arguments which are added to ChromeOptions  */
    /******************************************************************************/
    private void setChromeArguments()
    {
        try {
            if (webDriverOptions.getString("chrome.addArguments") != null) {
                chromeOptions.addArguments(webDriverOptions.getList("chrome.addArguments"));
                chromeOptions.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
                chromeOptions.setExperimentalOption("useAutomationExtension", false);
                //chromeOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);
                Map<String, Object> prefs = new HashMap<String, Object>();
                prefs.put("credentials_enable_service", false);
                prefs.put("profile.password_manager_enabled", false);
                prefs.put("download.default_directory", "C:\\Projects\\BluQore");

                prefs.put("profile.default_content_settings.popups", 0);
                prefs.put("pdfjs.disabled", true);
                chromeOptions.setExperimentalOption("prefs", prefs);
            }
        } catch (NullPointerException npe) {

            logger.info("chrome.addArguments empty in appication.properties");
        }
    }

    /******************************************************************************/
    /* This method sets headless ChromeOptions. You can run -Dheadless=true/false */
    /* using maven command. If you don't specify headless by default, it will use */
    /* the value present in application.properties file.                          */
    /******************************************************************************/
    private void setHeadless()
    {
        if(System.getProperty("headless") != null)
            chromeOptions.setHeadless(Boolean.parseBoolean(System.getProperty("headless")));
        else
            chromeOptions.setHeadless(Boolean.parseBoolean(webDriverOptions.getString("headless")));
    }

    /*******************************************************************************/
    /* This method sets chrome extensions in ChromeOptions. You should pass chrome */
    /* extensions by comma separated value in application properties file.         */
    /*******************************************************************************/
    private void setChromeExtensions() {
        try {
            if (!webDriverOptions.getString("chrome.addExtensions").equals("")) {
                chromeOptions.addExtensions(webDriverOptions.getList("chrome.addExtensions"));
            }
        } catch (NullPointerException npe) {
            logger.info("chrome.addArguments empty in appication.properties");
        }
    }
}