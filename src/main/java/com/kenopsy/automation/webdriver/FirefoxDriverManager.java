package com.kenopsy.automation.webdriver;

import org.openqa.selenium.firefox.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Iterator;

public class FirefoxDriverManager extends DriverManager {

    private static Logger logger = LoggerFactory.getLogger(FirefoxDriverManager.class);
    private FirefoxOptions firefoxOptions = new FirefoxOptions();

    /******************************************************************************/
    /* This constructor sets Gecko Web Driver URL from application.properties     */
    /* It also sets zip filename and webdriver executable file name. These values */
    /* and sets executable file name. These names are used to download WebDriver  */
    /******************************************************************************/
    public FirefoxDriverManager() {
        System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
        System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
        if(getOsName().contains("windows"))
        {
            setURL(webDriverOptions.getString("firefox.windows.url"));
            setEXEFileName(new File(project + "geckodriver.exe"));
            setZIPFilename(new File(project + "firefox.zip"));
        }
        else if(getOsName().contains("linux"))
        {
            setURL(webDriverOptions.getString("firefox.linux.url"));
            setEXEFileName(new File(project + webDriverOptions.getString("firefox.linux.driverFile")));
            setZIPFilename(new File("firefox.zip"));
        }
        createDriver();
    }

    /******************************************************************************/
    /* This Method creates gecko WebDriver Object. This instance will be created  */
    /* during initialization of spring context. Refer getWebDriver() method in    */
    /* Spring configuration file springconfig/CucumberContext.java file           */
    /******************************************************************************/
    @Override
    public void createDriver() {
        downloadDrivers();
        setFirefoxOptions();
        setHeadless();
        driver = new FirefoxDriver(firefoxOptions);
    }

    /******************************************************************************/
    /* This Method used to set chrome arguments which are added to FirefoxOptions */
    /******************************************************************************/
    private void setFirefoxOptions() {
        Iterator iterator = webDriverOptions.getKeys();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            if (key.contains("firefox.profile.preference")) {
                firefoxOptions.addPreference(key.replace("firefox.profile.preference.", ""),
                        webDriverOptions.getString(key));
            }
        }
        System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
    }

    /******************************************************************************/
    /* This method sets headless FirefoxOptions. You can run -Dheadless=true/false*/
    /* using maven command. If you don't specify headless by default, it will use */
    /* the value present in application.properties file.                          */
    /******************************************************************************/
    private void setHeadless()
    {
        if(System.getProperty("headless") != null)
            firefoxOptions.setHeadless(Boolean.parseBoolean(System.getProperty("headless")));
        else
            firefoxOptions.setHeadless(Boolean.parseBoolean(webDriverOptions.getString("headless")));
    }

}