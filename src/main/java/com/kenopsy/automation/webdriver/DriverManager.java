package com.kenopsy.automation.webdriver;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


public abstract class DriverManager {

    private static Logger logger = LoggerFactory.getLogger(DriverManager.class);

    private String driverDownloadURL;
    private File driverFile;
    private File driverZipFile;
    WebDriver driver;
    private InputStream inputStream;
    String project = System.getProperty("user.dir") + File.separator;
    PropertiesConfiguration webDriverOptions;
    URL url = null;

    protected abstract void createDriver();

    public DriverManager() {
        try {
            webDriverOptions = new PropertiesConfiguration("application.properties");
        } catch (Exception e) {
            logger.info("Unable to get the environment file");
            e.printStackTrace();
        }
    }

    /******************************************************************************/
    /* This Method sets the browser specific WebDriver download URL.              */

    /******************************************************************************/
    void setURL(String driverDownloadURL) {
        this.driverDownloadURL = driverDownloadURL;
        try {
            this.url = new URL(driverDownloadURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /******************************************************************************/
    /* This Method sets the zip file name for WebDriver. This value is            */
    /* configured by fetching the value from application.properties file.         */

    /******************************************************************************/
    public void setZIPFilename(File driverZipFile) {
        this.driverZipFile = driverZipFile;
    }

    /******************************************************************************/
    /* This Method sets the executable file name for WebDriver. This value is     */
    /* configured by fetching the value from application.properties file.         */

    /******************************************************************************/
    void setEXEFileName(File driverFile) {
        this.driverFile = driverFile;
    }
    public String getEXEFilePath() {
        return driverFile.getAbsolutePath();
    }

    /*****************************************************************************/
    /* This method returns the Operating system of the machine                   */

    /*****************************************************************************/

    String getOsName() {
        return System.getProperty("os.name").toLowerCase();
    }

    /*****************************************************************************/
    /* This method sets the URL to download the file from internet.              */
    /*****************************************************************************/
    private void setURLInputStream() {
        try {
            url = new URL(driverDownloadURL);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
                inputStream = httpURLConnection.getInputStream();
            else
                inputStream = httpURLConnection.getErrorStream();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /*****************************************************************************/
    /* This is actual method which downloads the WebDriver executable file from  */
    /* Internet. It only downloads when driver file not exists in the project.   */
    /*****************************************************************************/
    protected void downloadDrivers() {
        if (!driverFile.exists()) {
            try {
                setURLInputStream();
                OutputStream outputStream = new FileOutputStream(driverZipFile);
                byte[] buffer = new byte[8 * 1024];
                int bytesRead;
                while ((bytesRead = inputStream.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                logger.info("File downloaded: " + driverZipFile.getAbsolutePath());
                outputStream.close();
                inputStream.close();
                if (!driverZipFile.getName().contains(".exe"))
                    unZipFile();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    /*****************************************************************************/
    /* This method unzips download WebDriver zip file internet.                  */
    /*****************************************************************************/
    private void unZipFile() {
        try {
            byte[] buffer = new byte[8 * 1024];
            ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(driverZipFile.getAbsolutePath()));
            ZipEntry zipEntry = zipInputStream.getNextEntry();
            while (zipEntry != null) {
                String fileName = zipEntry.getName();
                File newFile = new File(project + File.separator + fileName);
                logger.info("Unzipping driver folder : " + newFile.getAbsoluteFile());
                new File(newFile.getParent()).mkdirs();
                FileOutputStream fos = new FileOutputStream(newFile);
                int length;
                while ((length = zipInputStream.read(buffer)) > 0) {
                    fos.write(buffer, 0, length);
                }
                newFile.setExecutable(true);
                fos.close();
                zipEntry = zipInputStream.getNextEntry();
            }
            zipInputStream.closeEntry();
            zipInputStream.close();
            logger.info("Finished downloading of drivers");
            driverZipFile.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected File GetDriverFile() {
        downloadDrivers();
        return driverFile;
    }

    public WebDriver getDriver() {
        if (null == driver) {
            logger.error("Driver is null, creating driver instance");
        }
        return driver;
    }
}