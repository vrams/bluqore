package com.kenopsy.automation.webdriver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DriverManagerFactory {

    private static Logger logger = LoggerFactory.getLogger(DriverManagerFactory.class);
    public static DriverManager getManager(String browser)
    {
        if(System.getProperty("browser") != null)
        {
            browser = System.getProperty("browser");
        }
        DriverManager driverManager;
        switch (browser) {
            case "chrome":
                driverManager = new ChromeDriverManager();
                break;
            case "firefox":
                driverManager = new FirefoxDriverManager();
                break;
            default:
                driverManager = new ChromeDriverManager();
                break;
        }
        return driverManager;

    }
}