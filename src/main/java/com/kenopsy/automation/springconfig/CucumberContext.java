package com.kenopsy.automation.springconfig;

import com.kenopsy.automation.webdriver.DriverManagerFactory;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@ComponentScan(basePackages = "com.kenopsy")
@PropertySource("classpath:application.properties")
public class CucumberContext {

    @Autowired
    Environment environment;

    @Value("${default.browser}")
    private String browser;

    @Bean(name = "webdriver", destroyMethod = "quit")
    @Scope("cucumber-glue")
    public WebDriver getWebDriver() {
        WebDriver webdriver = null;
        webdriver = DriverManagerFactory.getManager(browser).getDriver();
        return webdriver;
    }

    @Lazy(true)
    @Bean("browser")
    public String getBrowser() {
        return browser;
    }

    @Bean
    public DataSource dataSource()
    {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");

        if (System.getProperty("environment") == null)
        {
            dataSource.setUrl(environment.getProperty("dev.db.url"));
            dataSource.setUsername(environment.getProperty("dev.db.username"));
            dataSource.setPassword(environment.getProperty("dev.db.password"));
        }
        else if (System.getProperty("environment").equalsIgnoreCase("dev"))
        {
            dataSource.setUrl(environment.getProperty("dev.db.url"));
            dataSource.setUsername(environment.getProperty("dev.db.username"));
            dataSource.setPassword(environment.getProperty("dev.db.password"));
        }
        else if (System.getProperty("environment").equalsIgnoreCase("qa"))
        {
            dataSource.setUrl(environment.getProperty("qa.db.url"));
            dataSource.setUsername(environment.getProperty("qa.db.username"));
            dataSource.setPassword(environment.getProperty("qa.db.password"));
        }
        else if (System.getProperty("environment").equalsIgnoreCase("sit"))
        {
            dataSource.setUrl(environment.getProperty("sit.db.url"));
            dataSource.setUsername(environment.getProperty("sit.db.username"));
            dataSource.setPassword(environment.getProperty("sit.db.password"));
        }
        else if (System.getProperty("environment").equalsIgnoreCase("local"))
        {
            dataSource.setUrl(environment.getProperty("local.db.url"));
            dataSource.setUsername(environment.getProperty("local.db.username"));
            dataSource.setPassword(environment.getProperty("local.db.password"));
        }
        return dataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        return jdbcTemplate;
    }
}
