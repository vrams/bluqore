package com.kenopsy.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Scope("cucumber-glue")
public class HomePage extends BasePage {
    private static Logger logger = LoggerFactory.getLogger(EmployeesPage.class);

    public HomePage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "/html/body/app-root/home-comp/div/header/div[3]/label")
    WebElement accountName;

    @FindBy(xpath = "/html/body/app-root/home-comp/div/header/div[3]/a")
    WebElement logoutLink;

    @FindBy(xpath = "/html/body/app-root/home-comp/div/header/div[1]")
    WebElement openorderLogo;

    @FindAll({@FindBy(xpath = "/html/body/app-root/home-comp/div/header/div[2]/a")})
    List<WebElement> navigationTabs;

    @FindAll({@FindBy(xpath = "//div[@class=\"branding\"]")})
    List<WebElement> pageHeaderLogo;

    @FindBy(xpath = "//s-logout-pg/div/h3")
    WebElement loggedOutMessage;

    public void isHomePageLogoDisplayed()
    {
        waitForPageToLoad();
        waitForVisibilityOf(openorderLogo);
        logger.info("Open order Logo displayed on the page");
    }

    public void clickAccountName() {
        waitForVisibilityOf(accountName);
        accountName.click();
        logger.info("Clicked on Account Name");
    }

    public void clickNavigationTab(String navigationPage)
    {
        waitForVisibilityOfAllElements(navigationTabs, "Unable to find Navigation Tabs");
        for (WebElement navigationTab : navigationTabs) {
            if (navigationTab.getText().equalsIgnoreCase(navigationPage)) {
                navigationTab.click();
                waitForPageToLoad();
                logger.info("Clicked on navigation page: " + navigationTab.getText());
            }
        }
    }

    public void clickLogoutLink()
    {
        logger.info("Click Logout Link");
        clickWebElement(logoutLink);
    }

    public boolean verifyHomePageLoaded() {
        return isDesiredPageLoaded(pageHeaderLogo);
    }

    public boolean isLogoutScreenVisible()
    {
        return isElementVisible(loggedOutMessage);
    }

}
