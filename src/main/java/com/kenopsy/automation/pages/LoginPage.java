package com.kenopsy.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Scope("cucumber-glue")
public class LoginPage extends BasePage {

    private static Logger logger = LoggerFactory.getLogger(LoginPage.class);

    public LoginPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "/html/body/app-root/s-login-pg/div/form/input[1]")
    WebElement loginTextBox;

    @FindBy(xpath = "/html/body/app-root/s-login-pg/div/form/input[2]")
    WebElement passwordTextBox;

    @FindBy(xpath = "/html/body/app-root/s-login-pg/div/form/button")
    WebElement loginButton;

    @FindAll({@FindBy(xpath = "//div[@class='s-login-pg-head']")})
    List<WebElement> loginLogo;

    public void openApplication(String url) {
        logger.info("Opening Application with URL: " + url);
        driver.manage().window().maximize();
        driver.get(url);
        waitForPageToLoad();
    }

    public void inputUsername(String username) {
        inputText(loginTextBox, username);
        logger.info("Input login username:" + username);
    }

    public void inputPassword(String password) {
        inputText(passwordTextBox, password);
        logger.info("Input login password" + password);
    }

    public void clickLoginButton() {
        waitForVisibilityOf(loginButton);
        waitForElementToBeClickable(loginButton);
        clickWebElement(loginButton);
        logger.info("Clicked on login button");
    }

    public boolean verifyLoginPageLoaded() {
        return isDesiredPageLoaded(loginLogo);
    }
}
