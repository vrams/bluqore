package com.kenopsy.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**

 */

@Component
@Scope("cucumber-glue")
public class ProductsPage extends BasePage
{
    private static Logger logger = LoggerFactory.getLogger(ProductsPage.class);

    public ProductsPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindAll({@FindBy(xpath = "//datatable-row-wrapper/datatable-body-row/div[2]")})
    List<WebElement> productCodes;


    public boolean enableDisableProducts(HashMap<String, String> data)
    {
        boolean result = false;
        boolean toggle = false;
        if (data.get("Discontinued").equalsIgnoreCase("ON"))
        {
            toggle = true;
        }
        for (WebElement productCode: productCodes)
        {
            if(productCode.findElement(By.xpath("./datatable-body-cell[1]")).getText().equalsIgnoreCase(data.get("ProductCode")))
            {
                if((productCode.findElements(By.xpath("./datatable-body-cell[9]/div/div/input[@checked='true']")).size() == 0) && toggle)
                    productCode.findElement(By.xpath("./datatable-body-cell[9]")).click();
                else
                    productCode.findElement(By.xpath("./datatable-body-cell[9]")).click();

                logger.info("Clicked on the Discontinued button");
                result = true;

            }
        }
        return result;
    }
}
