package com.kenopsy.automation.pages;

import org.apache.commons.collections.list.TreeList;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**

 */

@Component
@Scope("cucumber-glue")
public class OrdersPage extends BasePage
{
    private static Logger logger = LoggerFactory.getLogger(OrdersPage.class);

    public OrdersPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindAll({@FindBy(xpath = "//datatable-body-cell/div/a")})
    List<WebElement> orderIDs;

    @FindAll({@FindBy(xpath = "//table[@class='s-order-line table']/tbody/tr")})
    List<WebElement> orderRows;

    @FindAll({@FindBy(xpath = "//*[@id='clr-tab-content-0']/table/thead/tr/th")})
    List<WebElement> tableHeader;

    @FindBy(xpath = "//s-orders-pg/div/form/h3")
    WebElement orderId;

    public void clickOrderID(String orderID)
    {
        waitForVisibilityOfAllElements(orderIDs, "Unable to find Order IDs");
        for (WebElement order: orderIDs)
        {
            if(order.getText().trim().equalsIgnoreCase(orderID)) {
                order.click();
                break;
            }
        }
    }

    public List<Map<String, Object>> getTableData()
    {
        waitForVisibilityOfAllElements(orderRows, "Unable to load orders data");
        List<String> tableHead = new TreeList();
        for (WebElement tHead: tableHeader)
        {
            if(!tHead.getText().isEmpty())
                tableHead.add(tHead.getText().replaceAll("\\s+",""));
        }

        List<Map<String, Object>> orderTable = new ArrayList<>();
        for(WebElement orderRow: orderRows)
        {
            Map<String, Object> tableData = new TreeMap<>();
            List<WebElement> tableCells = orderRow.findElements(By.xpath("./td"));
            for (int i = 0;i < tableCells.size();i++)
            {
                if(!tableCells.get(i).getText().isEmpty())
                tableData.put(tableHead.get(i),tableCells.get(i).getText());
            }
            orderTable.add(tableData);

        }
        return orderTable;
    }

    public boolean isOrderIdDisplayed()
    {
        return isElementVisible(orderId);
    }
}
