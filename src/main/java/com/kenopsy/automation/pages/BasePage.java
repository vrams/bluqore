package com.kenopsy.automation.pages;

import com.kenopsy.automation.webdriver.DriverManagerFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Base Class for creating driver instance which can be extended by other classes
 */
@Component
@Scope("cucumber-glue")
public class BasePage {

    private static Logger logger = LoggerFactory.getLogger(BasePage.class);
    WebDriver driver;
    private int waitTimeOut = 5;

    By saveImage = By.xpath("//div[@id='mask-img']");
    By loadingImage = By.xpath("//*[@id=\"mask\"]");

    /* Base page constructor */
    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

    }

    public void createWebDriver()
    {
        driver = DriverManagerFactory.getManager("chrome").getDriver();
    }
    /* Returns the web driver instance */
    public WebDriver getDriver() {
        return this.driver;
    }

    /* Kill WebDriver Instance */
    public void killWebDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

    /* Function to wait for visibility of the element to be displayed*/
    void waitForVisibilityOf(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, waitTimeOut);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    /*Wait for visibility for all the web elements */
    void waitForVisibilityOfAllElements(List<WebElement> elements, String message) {
        WebDriverWait wait = new WebDriverWait(driver, waitTimeOut);
        wait.until(ExpectedConditions.visibilityOfAllElements(elements));
    }

    /* Function to wait for visibility of the element to be displayed*/
    void waitForVisibilityOf(WebElement element, String errorMessageWhenNotFound) {
        WebDriverWait wait = new WebDriverWait(driver, waitTimeOut);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    /* Function to wait for visibility of the element to be displayed by passing the desired time*/
    void waitForVisibilityOf(WebElement element, int time) {
        WebDriverWait wait = new WebDriverWait(driver, time); // Driver wait time
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    /* Wait for the element to appear overloaded by By */
    void waitForVisibilityOfElementLocated(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, waitTimeOut);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    /* Wait for the element to appear overloaded by By with exception message */
    void waitForVisibilityOfElementLocated(By locator, String errorMessageWhenNotFound) {
        WebDriverWait wait = new WebDriverWait(driver, waitTimeOut);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    /* Wait for the element to disappear overloaded by By */
    void waitForInvisibilityOfElementLocated(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, waitTimeOut);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    /* Wait for the element to be clickable message */
    void waitForElementToBeClickable(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, waitTimeOut);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    /* Wait for the element to be clickable exception message */
    void waitForElementToBeClickable(WebElement element, String errorMessageWhenNotFound) {
        WebDriverWait wait = new WebDriverWait(driver, waitTimeOut);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    /* Function to wait for presence of the element */
    void waitForPresenceOfElementLocated(By by) {
        WebDriverWait wait = new WebDriverWait(driver, waitTimeOut);
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    /* Wait for the Alert to display */
    void waitForAlertIsPresent() {
        WebDriverWait wait = new WebDriverWait(driver, waitTimeOut);
        wait.until(ExpectedConditions.alertIsPresent());
    }

    /* Function to wait for the page to load. otherwise it will fail the test*/
    public void waitForPageToLoad() {
        ExpectedCondition<Boolean> javascriptDone = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                try {
                    return ((JavascriptExecutor) getDriver()).executeScript("return document.readyState").equals("complete");
                } catch (Exception e) {
                    return Boolean.FALSE;
                }
            }
        };
        WebDriverWait wait = new WebDriverWait(getDriver(), waitTimeOut);
        wait.until(javascriptDone);
    }

    /* Function to wait for the page to load. otherwise it will fail the test*/
    void waitForPageToLoad(int waitDuration) {
        ExpectedCondition<Boolean> expectation = driver -> {
            return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
        };
        try {
            Thread.sleep(500);
            new WebDriverWait(driver, waitDuration).until(expectation);
        } catch (Throwable error) {
            logger.warn("Timeout while waiting for Page Load Request to complete");
        }
    }

    /* Function to impose wait for desired milli seconds of time */
    public void waitFor(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException ie) {
            ie.getMessage();
        }
    }

    /*Function for staleness of the element*/
    void waitForStalenessOf(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, waitTimeOut);
        wait.until(ExpectedConditions.stalenessOf(element));
    }


    void reloadPage() {
        driver.navigate().refresh();
        waitForPageToLoad();
    }

    /* Retry WebElement Clicking until it get succeed. Overloaded by By*/
    boolean retryFindClick(By by) {
        boolean success = false;
        int clickAttempts = 0;
        while (clickAttempts < 30) {
            try {
                driver.findElement(by).click();
                success = true;
                break;
            } catch (StaleElementReferenceException e) {
                logger.info("Caught Stale Element Reference");
            }
            clickAttempts++;
        }
        return success;

    }
    /* Retry WebElement Clicking until it get succeed. Overloaded by WebElement*/
    boolean retryFindClick(WebElement element) {
        boolean success = false;
        int clickAttempts = 0;
        while (clickAttempts < 30) {
            try {
                element.click();
                success = true;
                break;
            } catch (StaleElementReferenceException e) {
                logger.info("Caught Stale Element Reference");
            }
            clickAttempts++;
        }
        return success;

    }
    /* Methods for scrolling the pages */
    void scrollPageDown() {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        //executor.executeScript("window.scrollBy(0,250)", "");
        executor.executeScript("scroll(0, 350);");
    }

    void scrollPageUp() {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("scroll(0, -350);");

    }

    void scrollPageLeft() {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("scroll(-300, 0);");

    }

    public void scrollToBottom() {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("window.scrollTo(0,document.body.scrollHeight)");
    }

    /* Method for action builder */
    void performAction(WebElement element, String value, boolean tab) {
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.sendKeys(value).perform();
        //actions.build().perform();
        if (tab) {
            actions.sendKeys(Keys.TAB).perform();
        }
    }

    /* Method for selecting the dropdown text */
    void selectDropdownText(WebElement dropdown, String selectText) {
        Select select = new Select(dropdown);
        select.selectByVisibleText(selectText);
    }

    /* Method for inputText in the textbox */
    void inputText(WebElement webElement, String text) {
        waitForVisibilityOf(webElement);
        webElement.sendKeys(text);

    }

    /* Click on any link or web element on the page*/
    void clickWebElement(WebElement webElement) {
        waitForVisibilityOf(webElement);
        webElement.click();
        waitForPageToLoad();
    }

    /* This method is used to verify whether the desired page has been loaded or not */

    boolean isDesiredPageLoaded(List<WebElement> webElements)
    {
        waitForPageToLoad();
        boolean isPageLoaded = false;
        if (webElements.size() != 0)
            isPageLoaded = true;
        return  isPageLoaded;
    }

    boolean isElementVisible(WebElement webElement)
    {
        waitForVisibilityOf(webElement);
        return webElement.isDisplayed();
    }

}
