package com.kenopsy.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**

 */

@Component
@Scope("cucumber-glue")
public class EmployeesPage extends BasePage {

    private static Logger logger = LoggerFactory.getLogger(EmployeesPage.class);
    public EmployeesPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindAll({@FindBy(xpath = "//datatable-body-row/div[2]")})
    List<WebElement> dataTableRows;

    public Map<String, List<String>> getEmployeeTableData()
    {
        waitForVisibilityOfAllElements(dataTableRows, "Employee rows not loaded");
        TreeMap<String, List<String>> employeeTableData = new TreeMap<>();
        System.out.println("Count of all employees: " + dataTableRows.size());
        for (WebElement dataTableRow : dataTableRows)
        {
            List<WebElement> tableCells = dataTableRow.findElements(By.xpath("./datatable-body-cell/div"));
            String key = "";
            List<String> employeeData = new ArrayList<>();
            for (int i = 0; i< tableCells.size();i++)
            {
                WebElement tableCell = tableCells.get(i);
                if(i == 0)
                {
                    key = tableCell.getText();
                }
                else
                {
                    employeeData.add(tableCell.getText());
                }
            }
            employeeTableData.put(key,employeeData);

        }
        return employeeTableData;
    }

}
