package com.kenopsy.automation.library;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by vikramt on 3/08/2019.
 */
@Component
public class ExcelData {
    private static Logger logger = LoggerFactory.getLogger(ExcelData.class);
    public List<HashMap<String, String>> getDataByFilePath(String filePath) {
        List<HashMap<String, String>> mydata = new ArrayList<>();
        try {
            FileInputStream fs = new FileInputStream(filePath);
            XSSFWorkbook workbook = new XSSFWorkbook(fs);
            XSSFSheet sheet = workbook.getSheetAt(1);
            Row HeaderRow = sheet.getRow(0);
            for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
                Row currentRow = sheet.getRow(i);
                HashMap<String, String> currentHash = new HashMap<String, String>();
                for (int j = 0; j < currentRow.getPhysicalNumberOfCells(); j++) {
                    Cell currentCell = currentRow.getCell(j);
                    switch (currentCell.getCellType()) {
                        case STRING:
                            currentHash.put(HeaderRow.getCell(j).getStringCellValue(), currentCell.getStringCellValue());
                            break;
                    }
                }
                mydata.add(currentHash);
            }
            fs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mydata;
    }

    public List<HashMap<String, String>> getDataByFileSheetRow(String filePath, String workSheet, int sheetRow) {
        List<HashMap<String, String>> mydata = new ArrayList<>();
        try {
            FileInputStream fs = new FileInputStream(filePath);
            XSSFWorkbook workbook = new XSSFWorkbook(fs);
            XSSFSheet sheet = workbook.getSheet(workSheet);
            Row HeaderRow = sheet.getRow(0);
                Row currentRow = sheet.getRow(sheetRow);
                HashMap<String, String> currentHash = new HashMap<String, String>();
                for (int j = 0; j < currentRow.getPhysicalNumberOfCells(); j++)
                {
                    Cell currentCell = currentRow.getCell(j);
                    switch (currentCell.getCellType()) {
                        case STRING:
                            currentHash.put(HeaderRow.getCell(j).getStringCellValue(), currentCell.getStringCellValue());
                            break;
                    }
                mydata.add(currentHash);
            }
            fs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mydata;
    }

    public HashMap<String, String> getData(String filePath, int rowIndex) {
        HashMap<String, String> rowData = new HashMap<String, String>();
        try {
            FileInputStream fs = new FileInputStream(filePath);
            XSSFWorkbook workbook = new XSSFWorkbook(fs);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Row HeaderRow = sheet.getRow(0);
            Row currentRow = sheet.getRow(rowIndex);

            for (int j = 0; j < currentRow.getPhysicalNumberOfCells(); j++)
            {
                Cell currentCell = currentRow.getCell(j);
                switch (currentCell.getCellType()) {
                    case STRING:
                        rowData.put(HeaderRow.getCell(j).getStringCellValue(), currentCell.getStringCellValue());
                        break;
                }
            }

            fs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowData;
    }

}
