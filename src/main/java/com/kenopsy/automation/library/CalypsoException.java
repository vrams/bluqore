package com.kenopsy.automation.library;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by vthaduri on 23/01/2017.
 */
public class CalypsoException extends Exception
{
    private static Logger logger = LoggerFactory.getLogger(CalypsoException.class);
    public CalypsoException(String message)
    {
        super(message);
        logger.error(message);
    }
    public CalypsoException(Throwable cause) {
    }
}
