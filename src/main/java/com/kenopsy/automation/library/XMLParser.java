package com.kenopsy.automation.library;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.Node;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;

@Component
public class XMLParser {

    private DocumentBuilderFactory dbFactory;
    private DocumentBuilder documentBuilder;
    private Document document;
    private XPath xpath;
    private PropertiesConfiguration pc;
    private NodeList nodeList;
    private Node node;
    private static Logger logger = LoggerFactory.getLogger(XMLParser.class);

    public XMLParser() {
        try {
            dbFactory = DocumentBuilderFactory.newInstance();
            documentBuilder = dbFactory.newDocumentBuilder();
            xpath = XPathFactory.newInstance().newXPath();

            pc = new PropertiesConfiguration("application.properties");
        } catch (Exception e) {
            logger.error("Unable to find Properties file");
            e.printStackTrace();
        }
    }

    //Parses the SOAP response and returns the desired value from the supplied xpath expression
    public NodeList parseResponse(String response, String xpathExpression) throws CalypsoException {
        try {
            document = documentBuilder.parse(new org.xml.sax.InputSource(new StringReader(response)));
            document.getDocumentElement().normalize();
            nodeList = (NodeList) xpath.compile(xpathExpression).evaluate(document, XPathConstants.NODESET);
            if (nodeList.getLength() == 0 || nodeList == null) {
                logger.info("Error in Xpath Expression. Xpath doesn't exist");
                // throw new MDMException("FATAL Error in SOAP UI Response");
            }
        } catch (NullPointerException e) {
            throw new CalypsoException("SOAPUI Response is Empty");
        } catch (XPathExpressionException e) {
            throw new CalypsoException("Invalid Xpath or Xpath doesn't exist");
        } catch (IOException e) {
            throw new CalypsoException("Invalid XML Response or unable to parse the XML Document");
        } catch (SAXException e) {
            throw new CalypsoException("XML Document parsing exception");
        }
        return nodeList;
    }

    public String getElement(String response, String xpath) throws CalypsoException {
        return parseResponse(response, xpath).item(0).getFirstChild().getNodeValue();
    }

    public int getNodeCount(String response, String xpath) throws Exception {
        return parseResponse(response, xpath).getLength();
    }

    public String getNodeElement(String response, String xpathExpression) throws CalypsoException {
        String element = "";
        try {
            document = documentBuilder.parse(new org.xml.sax.InputSource(new StringReader(response)));
            document.getDocumentElement().normalize();
            node = (Node) xpath.evaluate(xpathExpression, document, XPathConstants.NODE);
            if (node == null)
                element = "NULL";
            else
                element = node.getFirstChild().getNodeValue();

        } catch (NullPointerException e) {
            throw new CalypsoException("SOAPUI Response is Empty");
        } catch (XPathExpressionException e) {
            throw new CalypsoException("Invalid Xpath or Xpath doesn't exist");
        } catch (IOException e) {
            throw new CalypsoException("Invalid XML Response or unable to parse the XML Document");
        } catch (SAXException e) {
            throw new CalypsoException("XML Document parsing exception");
        }
        return element;
    }
}
