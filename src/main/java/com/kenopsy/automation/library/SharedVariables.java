package com.kenopsy.automation.library;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class SharedVariables {
    public Map<String, List<String>> employeeTable;
    public Map<String, List<String>> employeeResponse;
    public  String authToken = "";
    public String responseBody = "";
    public String orderId;
    public List<Map<String, Object>> orderTableUI;
    public List<Map<String, Object>> orderTableDB;
    public String url = "";
    public String apiURL = "";
    public String soapResponse = "";
}
