package com.kenopsy.automation.library;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class JsonParser {

    private static Logger logger = LoggerFactory.getLogger(JsonParser.class);
    /*Method to SET a value in Rest string using Rest path*/
    public String setValue(String jsonString, String jpath, String value)
    {
        DocumentContext documentContext = JsonPath.parse(jsonString);
        return documentContext.set(jpath,value).jsonString();
    }
    /*Method to GET a value in Rest string using Rest path*/
    public String getValue(String jsonString, String jpath)
    {
        return JsonPath.parse(jsonString).read(jpath).toString();
    }

    /*Method to create a value in Rest string using Rest path*/
    public String createElement(String jsonString, String jpath)
    {
        //TODO: Implement this as per your payload
        return "";
    }

    /*Get count of items in Rest string using Rest path*/
    public int getJsonArrayCount(String jsonString, String jpath)
    {
        JSONArray jsonArray = JsonPath.read(jsonString, "$.items.[*]");
        return jsonArray.size();
    }

}
