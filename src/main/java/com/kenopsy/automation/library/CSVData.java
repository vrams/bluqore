package com.kenopsy.automation.library;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.*;
import java.util.HashMap;

/**
 * Created by vikramt on 4/08/2019.
 */
@Component
public class CSVData {
    private static Logger logger = LoggerFactory.getLogger(CSVData.class);
    private static Connection connection = null;
    private Statement statement;
    private static final String jdbcDriver = "org.relique.jdbc.csv.CsvDriver";

    /******************************************************************************/
    /* This method sets the CSV File path and uses this as a table for querying   */
    /******************************************************************************/
    private void setCSVFile(String path) {
        try {
            Class.forName(jdbcDriver).newInstance();
            path = path.replace("/", "\\").substring(1, path.length());
            connection = DriverManager.getConnection("jdbc:relique:csv:" + path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /******************************************************************************/
    /* This method gets CSV Row. It takes arguments table and request.            */
    /******************************************************************************/
    public String getCSVRow(String table, String request) {
        setCSVFile(table);
        String response = "";
        String query = "select * from " + table + " where request = " + "'" + request + "'";
        try {
            statement = connection.createStatement();
            ResultSet results = statement.executeQuery(query);
            ResultSetMetaData meta = results.getMetaData();

            while (results.next()) {

                for (int i = 0; i < meta.getColumnCount(); i++) {
                    response = results.getString("response");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /******************************************************************************/
    /* This method gets CSV Row. It takes arguments filepath and search criteria. */
    /******************************************************************************/
    public HashMap<String, String> getRow(String filePath, String searchCriteria) throws IOException
    {
        HashMap<String, String> hashMaps = new HashMap<>();
        Reader reader = new FileReader(filePath);
        Iterable<CSVRecord> csvRecords = CSVFormat.DEFAULT
                .withFirstRecordAsHeader()
                .withAllowMissingColumnNames()
                .parse(reader);

        for (CSVRecord record : csvRecords)
        {
            HashMap<String,String> headerMap = (HashMap<String, String>) record.toMap();
            if(headerMap.values().contains(searchCriteria))
            {
                hashMaps = headerMap;
                break;
            }
        }
        return hashMaps;
    }

}