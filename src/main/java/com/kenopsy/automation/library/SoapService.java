package com.kenopsy.automation.library;

import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.impl.wsdl.WsdlTestSuite;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCase;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCaseRunner;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStepResult;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStep;
import com.eviware.soapui.model.testsuite.TestStepResult;
import com.eviware.soapui.support.types.StringToObjectMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class SoapService {

    @Autowired
    Environment environment;

    String response = "";

    public String getSoapResponse(String code) {
        try {
            //Prepare the SoapUI Project
            WsdlProject project = new WsdlProject(getClass().getClassLoader().getResource(environment.getProperty("SoapProject")).getFile());
            WsdlTestSuite wsdlTestSuite = project.getTestSuiteByName("GetBankDetails");
            WsdlTestCase wsdlTestCase = wsdlTestSuite.getTestCaseByName("GetBankDetails");
            WsdlTestStep wsdlTestStep = wsdlTestCase.getTestStepByName("GetBankDetails");
            //Set the end point URL based on the environment
            //wsdlTestStep.setPropertyValue("Endpoint", getEndpoint());
            //Set the Header Username and Password
            //wsdlTestCase.setPropertyValue("wsseUsername", getWsseUsername());
            //wsdlTestCase.setPropertyValue("wssePassword", getWssePassword());
            //Set the payload parameters (Other custom properties)
            wsdlTestCase.setPropertyValue("blzCode", code);
            WsdlTestCaseRunner wsdlTestCaseRunner = new WsdlTestCaseRunner(wsdlTestCase, new StringToObjectMap(wsdlTestCase.getProperties()));
            System.out.println(wsdlTestCaseRunner.toString());
            TestStepResult testStepResult = wsdlTestCaseRunner.runTestStep(wsdlTestStep);
            if (testStepResult instanceof WsdlTestRequestStepResult) {
                response = (((WsdlTestRequestStepResult) testStepResult).getResponse().getContentAsString());
            }
            project.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
