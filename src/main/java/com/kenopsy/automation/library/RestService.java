package com.kenopsy.automation.library;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class RestService {
    private static Logger logger = LoggerFactory.getLogger(RestService.class);

    RestTemplate restTemplate = new RestTemplate();
    String requestBody;
    HttpHeaders httpHeaders = new HttpHeaders();
    String responseBody;

    /* Set Request Body*/
    public String setRequestBody(String body)
    {
        requestBody = body;
        return requestBody;
    }

    public void setRequestHeader(String headerName, String headerValue)
    {
        httpHeaders.add(headerName,headerValue);
    }

    /*Method to SET a value in Rest string using Rest path*/
    public void setJsonNode(String jsonPath, String jsonValue)
    {
        DocumentContext documentContext = JsonPath.parse(requestBody);
        requestBody = documentContext.set(jsonPath, jsonValue).jsonString();
    }

    /*Method to GET a value in Rest string using Rest path*/
    public String getValue(String jsonString, String jpath)
    {
        return JsonPath.parse(jsonString).read(jpath).toString();
    }

    private void resetRequest()
    {
        requestBody = "";
        httpHeaders.clear();
    }

    /* Method to post the request body with headers */
    public String postRequest(String url)
    {
        HttpEntity<String> httpEntity = new HttpEntity<String>(requestBody,httpHeaders);
        responseBody = restTemplate.exchange(url, HttpMethod.POST,httpEntity,String.class).getBody();
        resetRequest();
        return responseBody;
    }

    public String queryParamUriBuilder(String url, String key, String value)
    {
        return UriComponentsBuilder.fromHttpUrl(url).queryParam(key, value).toUriString();
    }

    /* Method to get the response with headers */
    public String getRequest(String url, HttpHeaders httpHeaders)
    {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> httpEntity = new HttpEntity<>(null,httpHeaders);
        return restTemplate.exchange(url, HttpMethod.GET, httpEntity,String.class).getBody();
    }
    /* Method to get the response with headers */
    public String getRequest(String url)
    {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> httpEntity = new HttpEntity<>(null,httpHeaders);
        responseBody = restTemplate.exchange(url, HttpMethod.GET, httpEntity,String.class).getBody();
        resetRequest();
        return responseBody;
    }
}
