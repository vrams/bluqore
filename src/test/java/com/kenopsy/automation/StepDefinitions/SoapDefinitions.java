package com.kenopsy.automation.StepDefinitions;


import com.kenopsy.automation.library.CalypsoException;
import cucumber.api.java.en.And;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SoapDefinitions extends ParentSteps {

    private static Logger logger = LoggerFactory.getLogger(SoapDefinitions.class);


    @And("I get order id for the code {string} from SOAP Request")
    public void iGetTheOrderCodeForFromSOAPRequest(String code) throws CalypsoException {

        sharedVariables.soapResponse = soapService.getSoapResponse(code);
        sharedVariables.orderId = xmlParser.getElement(sharedVariables.soapResponse,
                "//getBankResponse/details/plz");
        Assert.assertFalse("Invalid Response",sharedVariables.orderId.isEmpty());
        logger.info("Soap Response: " + sharedVariables.soapResponse);
    }
}
