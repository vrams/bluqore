package com.kenopsy.automation.StepDefinitions;

import com.kenopsy.automation.library.CalypsoException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.cucumber.datatable.DataTable;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class RestDefinitions extends ParentSteps {

    private static Logger logger = LoggerFactory.getLogger(RestDefinitions.class);

    private String getJSONFile(String filename) throws CalypsoException{
        String fileToString = "";
        try {
            ClassLoader classLoader = this.getClass().getClassLoader();
            File file = new File(classLoader.getResource("Rest/" + filename).getFile());
            fileToString = FileUtils.readFileToString(file);
        } catch (IOException e) {
            throw new CalypsoException("Unable to find file: " + filename);
        }
        return fileToString;
    }

    @Given("^I create session token with (.*?) and (.*?)$")
    public void i_create_session_token_with_username_and_password(String username, String password) throws Throwable {
        restService.setRequestBody(getJSONFile("session.json"));
        restService.setJsonNode("$.username", username);
        restService.setJsonNode("$.password", password);
        restService.setRequestHeader("Accept", "*/*");
        restService.setRequestHeader("Content-Type", "application/Rest");
        sharedVariables.responseBody = restService.postRequest(sharedVariables.apiURL + "session");
        sharedVariables.authToken = jsonParser.getValue(sharedVariables.responseBody, "$.item.token");
        Assert.assertFalse("Invalid Response",sharedVariables.responseBody.isEmpty());
    }

    @Then("^I GET response from \"([^\"]*)\" with request params$")
    public void i_GET_response_from_with_request_params(String url, DataTable requestParams) throws Throwable {
        restService.setRequestHeader("Accept", "*/*");
        restService.setRequestHeader("Authorization", sharedVariables.authToken);
        List<Map<String, String>> data = requestParams.asMaps(String.class, String.class);
        url = restService.queryParamUriBuilder(sharedVariables.apiURL + url, data.get(0).get("Key"), data.get(0).get("Value"));
        sharedVariables.responseBody = restService.getRequest(url);
        Assert.assertFalse("Invalid Response",sharedVariables.responseBody.isEmpty());
    }


}
