package com.kenopsy.automation.StepDefinitions;

import com.kenopsy.automation.library.*;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public abstract class ParentSteps {

    private static Logger logger = LoggerFactory.getLogger(ParentSteps.class);

    @Autowired
    JsonParser jsonParser;

    @Autowired
    RestService restService;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    SharedVariables sharedVariables;

    @Autowired
    ExcelData excelData;

    @Autowired
    CSVData csvData;

    @Autowired
    SoapService soapService;

    @Autowired
    XMLParser xmlParser;

    PropertiesConfiguration environment;

    public ParentSteps() {
        try {
            environment = new PropertiesConfiguration("application.properties");
        } catch (Exception e) {
            logger.info("Unable to get the environment file");
            e.printStackTrace();
        }
    }

    private void setAPIUrl() {
        switch (System.getProperty("environment").toUpperCase()) {
            case "DEV":
                sharedVariables.apiURL = environment.getString("DEV_API");
                break;
            case "QA":
                sharedVariables.apiURL = environment.getString("QA_API");
                break;
            case "SIT":
                sharedVariables.apiURL = environment.getString("SIT_API");
                break;
            case "Local":
                sharedVariables.apiURL = environment.getString("Local_API");
                break;
        }
    }


    protected void setURL(String page) {
        switch (page.toUpperCase()) {
            case "ADMIN":
                switch (System.getProperty("environment").toUpperCase()) {
                    case "DEV":
                        sharedVariables.url = environment.getString("DEVAdmin");
                        break;
                    case "QA":
                        sharedVariables.url = environment.getString("QAAdmin");
                        break;
                    case "SIT":
                        sharedVariables.url = environment.getString("SITAdmin");
                        break;
                    case "Local":
                        sharedVariables.url = environment.getString("LocalAdmin");
                        break;
                }
                break;
            case "SITE":
                switch (System.getProperty("environment").toUpperCase()) {
                    case "DEV":
                        sharedVariables.url = environment.getString("DEVSite");
                        break;
                    case "QA":
                        sharedVariables.url = environment.getString("QASite");
                        break;
                    case "SIT":
                        sharedVariables.url = environment.getString("SITSite");
                        break;
                    case "Local":
                        sharedVariables.url = environment.getString("LocalSite");
                        break;
                }
                break;
        }
        logger.info("Application URL set to: " + sharedVariables.url);
        setAPIUrl();
    }

}
