package com.kenopsy.automation.StepDefinitions;

import com.kenopsy.automation.library.CalypsoException;
import com.kenopsy.automation.library.RestService;
import com.kenopsy.automation.springconfig.CucumberContext;
import com.kenopsy.automation.pages.*;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.junit.Assert;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static org.openqa.selenium.OutputType.BYTES;

@ContextConfiguration(classes = CucumberContext.class)
public class WebDefinitions extends ParentSteps {

    private static Logger logger = LoggerFactory.getLogger(WebDefinitions.class);

    @Autowired
    private LoginPage loginPage;

    @Autowired
    private HomePage homePage;

    @Autowired
    private EmployeesPage employeesPage;

    @Autowired
    private OrdersPage ordersPage;

    @Autowired
    ProductsPage productsPage;

    @Autowired
    private RestService restService;

    @Before
    public void init() {
        if (System.getProperty("environment") == null) {
            System.setProperty("environment", "DEV");
        }
        logger.info("Running tests in environment: " + System.getProperty("environment"));
    }

    @After
    public void tearDown(Scenario scenario) {
        if (scenario.isFailed()) {
            logger.error("Scenario failed to execute...");
            try {
                byte[] screenshot = ((TakesScreenshot) loginPage.getDriver()).getScreenshotAs(BYTES);
                scenario.embed(screenshot, "image/png");
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }


    private String getTestDataFile(String filename) throws Exception {
        ClassLoader classLoader = this.getClass().getClassLoader();
        File file = new File(classLoader.getResource("TestData/" + filename).getFile());
        return file.getAbsolutePath();
    }

    private String getTestDataFile() throws Exception {
        ClassLoader classLoader = this.getClass().getClassLoader();
        File file = new File(classLoader.getResource("TestData/InputData.xlsx").getFile());
        return file.getAbsolutePath();
    }

    @Given("^I open Openorder (.*?) application$")
    public void i_open_Openorder_administration_application(String page) {
        setURL(page);
        loginPage.openApplication(sharedVariables.url);
        Assert.assertTrue("Login Page not loaded", loginPage.verifyLoginPageLoaded());
    }

    @When("^I login as (.*?) with password as (.*?)$")
    public void i_login_application_with_username_and_password(String username, String password) throws CalypsoException {
        loginPage.inputUsername(username);
        loginPage.inputPassword(password);
        loginPage.waitForPageToLoad();
        loginPage.clickLoginButton();
        loginPage.waitFor(1000);
        Assert.assertTrue("Application Home Page not loaded", homePage.verifyHomePageLoaded());
    }

    @When("^I logout of Openorder Application$")
    public void i_logout_of_Openorder_Application() throws CalypsoException {
        homePage.isHomePageLogoDisplayed();
        homePage.clickAccountName();
        homePage.clickLogoutLink();
        Assert.assertTrue("Application Home Page not loaded", homePage.isLogoutScreenVisible());
    }


    @When("^I navigate to \"([^\"]*)\" page$")
    public void i_navigate_to_page(String page) throws Throwable {
        homePage.clickNavigationTab(page);
        Assert.assertTrue(page + " Page not loaded", homePage.verifyHomePageLoaded());
    }

    @Then("^I capture the employees list from employees page$")
    public void i_capture_the_employees_list_from_employees_page() throws Throwable {
        sharedVariables.employeeTable = employeesPage.getEmployeeTableData();
        Assert.assertFalse("Employer Table is Empty ", sharedVariables.employeeTable.isEmpty());
    }


    @Then("^I validate employees on UI against Restful service$")
    public void i_validate_employees_on_UI_against_Restful_service() throws Throwable {
        int jsonCount = jsonParser.getJsonArrayCount(sharedVariables.responseBody, "$.items.[*]");
        TreeMap<String, List<String>> employeeResponseData = new TreeMap<>();
        for (int i = 0; i < jsonCount; i++) {
            List<String> employeeData = new ArrayList<>();
            employeeData.add(jsonParser.getValue(sharedVariables.responseBody, "$.items.[" + i + "].firstName"));
            employeeData.add(jsonParser.getValue(sharedVariables.responseBody, "$.items.[" + i + "].lastName"));
            employeeData.add(jsonParser.getValue(sharedVariables.responseBody, "$.items.[" + i + "].email"));
            employeeData.add(jsonParser.getValue(sharedVariables.responseBody, "$.items.[" + i + "].phone"));
            employeeData.add(jsonParser.getValue(sharedVariables.responseBody, "$.items.[" + i + "].department"));
            String id = jsonParser.getValue(sharedVariables.responseBody, "$.items.[" + i + "].id");
            employeeResponseData.put(id, employeeData);
        }

        sharedVariables.employeeResponse = employeeResponseData;
        for (String key : sharedVariables.employeeTable.keySet()) {
            List<String> employee = new ArrayList<>();
            employee = sharedVariables.employeeResponse.get(key);
            for (int i = 0; i < employee.size(); i++) {
                String actualJsonResponse = employee.get(i);
                String expectedResponse = sharedVariables.employeeTable.get(key).get(i);
                Assert.assertEquals(actualJsonResponse, expectedResponse);
            }
        }
    }


    @Then("^I click on (.*?) OrderID in Orders page$")
    public void i_click_on_OrderID_in_Orders_page(String orderId) throws Throwable {
        sharedVariables.orderId = orderId;
        ordersPage.clickOrderID(orderId);
        Assert.assertTrue(orderId + " not visible", ordersPage.isOrderIdDisplayed());
    }

    @Then("^I record all the orders from the page$")
    public void i_record_all_the_orders_from_the_page() throws Throwable {
        sharedVariables.orderTableUI = ordersPage.getTableData();
        Assert.assertFalse("Order Table is Empty ", sharedVariables.orderTableUI.isEmpty());
    }

    @Then("^I validate orders against the database$")
    public void i_validate_orders_against_the_database() throws Throwable {
        String sql = " SELECT product_code as Code, order_item_status as LineStatus, product_name as ProductName, " +
                "category as Category, round(quantity) as Quantity, round(unit_price,2) as UnitPrice " +
                "FROM order_details WHERE order_id = ?";
        List<Map<String, Object>> database = jdbcTemplate.queryForList(sql, sharedVariables.orderId);
        for (int i = 0; i < database.size(); i++) {
            Map<String, Object> dataRow = database.get(i);
            for (String name : dataRow.keySet())
                Assert.assertEquals(sharedVariables.orderTableUI.get(i).get(name), dataRow.get(name).toString());
        }
    }

    @Then("^I Enable/Disable (\\d+) in \"([^\"]*)\" worksheet$")
    public void i_Enable_Disable_in_Products_xl(int workBookRow, String workSheet) throws Throwable {
        String filePath = getTestDataFile();
        productsPage.enableDisableProducts(excelData.getDataByFileSheetRow(filePath, workSheet, workBookRow).get(0));

    }

    @Then("^I Enable/Disable (.*?) in \"([^\"]*)\" csv$")
    public void i_Enable_Disable_in_Products_csv(String productCode, String csvFile) throws Throwable {
        String filePath = getTestDataFile(csvFile + ".csv");
        Assert.assertTrue("Product Code " + productCode + " doesn't exists",
                productsPage.enableDisableProducts(csvData.getRow(filePath, productCode)));

    }

}
