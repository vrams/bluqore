package com.kenopsy.automation;

import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by vthaduri on 27/01/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@CucumberOptions(
        monochrome = true,
        features = "@target/FailedScenarios.txt",
        plugin = {"pretty", "html:target/site/cucumber-pretty",
                "Rest:target/cucumber.Rest"}
        //,tags = "@sample"
)
public class FailedScenarios {

}
