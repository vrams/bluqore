package com.kenopsy.automation;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:features",
        plugin = {"pretty", "html:target/cucumber-reports",
                "junit:target/test-reports.xml",
                "json:target/cucumber.json",
                "rerun:target/FailedScenarios.txt"}
        //,tags = "@dev"
)
public class CukeRunner   {

}
