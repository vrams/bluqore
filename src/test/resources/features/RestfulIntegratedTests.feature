Feature: REST Integration Tests
  In order to add the product
  As a registered admin user
  I want to verify the new products are added

  @rest @database @employees @test
  Scenario: Validate Employees against Restful Service
    Given I open Openorder admin application
    When I login as admin with password as admin
    And I navigate to "Employees" page
    Then I capture the employees list from employees page
    Then I create session token with admin and admin
    And I GET response from "api/employees" with request params
      | Key  | Value |
      | Size | 20    |
    Then I validate employees on UI against Restful service
    And I logout of Openorder Application
