Feature: Excel Data Input Tests
  In order to add the product
  As a registered admin user
  I want to verify the new products are added

  @data @excel @products
  Scenario Outline: Read input data from Excel
    Given I open Openorder admin application
    When I login as admin with password as admin
    And I navigate to "Products" page
    Then I Enable/Disable <ProductCode> in "Products" worksheet
    Then I logout of Openorder Application
    Examples:
      | ProductCode |
      | 1           |
      | 2           |
      | 3           |
      | 4           |
      | 5           |
