Feature: CSV Data Input Tests
  In order to add the product
  As a registered admin user
  I want to verify the new products are added

  @data @csv @products
  Scenario Outline: Read input data from CSV
    Given I open Openorder admin application
    When I login as admin with password as admin
    And I navigate to "Products" page
    Then I Enable/Disable <ProductCode> in "Products" csv
    Then I logout of Openorder Application
    Examples:
      | ProductCode |
      | P2          |
      | P8          |
      | P15         |
      | P18         |

  @data @csv @products
  Scenario Outline: Read input data from CSV
    Given I open Openorder admin application
    When I login as admin with password as admin
    And I navigate to "Products" page
    Then I Enable/Disable <ProductCode> in "Products" csv
    Then I logout of Openorder Application
    Examples:
      | ProductCode |
      | P60         |
