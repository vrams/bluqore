Feature: Database Integration Tests
  In order to add the product
  As a registered admin user
  I want to verify the new products are added

  @database @orders @dev
  Scenario: Validate order information against Database
    Given I open Openorder site application
    When I login as demo with password as demo
    And I navigate to "Orders" page
    Then I click on 4015 OrderID in Orders page
    Then I record all the orders from the page
    Then I validate orders against the database
    And I logout of Openorder Application

